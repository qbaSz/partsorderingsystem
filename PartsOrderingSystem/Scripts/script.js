﻿$(document).on("click", "#btn-search-client", function () {
    var surname = $("#Surname_Input").val();
    var email = $("#EMail_Input").val();
    console.log(surname);
    $.ajax({
        url: "/Repair/FindClient",
        method: "POST",
        data: {surname : surname, email : email}
    }).done(function (response) {
        $(".clients-list").html(response);
    })
});

$(document).on("click", ".client-row", function () {
    $(".client-row").removeClass("client-row-selected");
    $(this).addClass("client-row-selected");
    var clientId = $(this).attr("id");
    console.log(clientId);
    $("#ClientId").val(clientId);
});

$(document).on("click", "#btn-search-part", function () {
    var carMake = $("#Part_CarMake").val();
    var carModel = $("#Part_CarModel").val();
    var carYear = $("#Part_CarYear").val();
    var partName = $("#Part_Name").val();
    console.log(carMake);
    console.log(carModel);
    $.ajax({
        url: "/Repair/FindPart",
        method: "POST",
        data: { carMake: carMake, carModel: carModel, carYear: carYear, partName: partName, inStock: true }
    }).done(function (response) {
        $(".parts-search-list").html(response);
    })
})

$(document).on("click", "#btn-search-part-order", function () {
    var carMake = $("#Part_CarMake").val();
    var carModel = $("#Part_CarModel").val();
    var carYear = $("#Part_CarYear").val();
    var partName = $("#Part_Name").val();
    console.log(carMake);
    console.log(carModel);
    $.ajax({
        url: "/Repair/FindPart",
        method: "POST",
        data: { carMake: carMake, carModel: carModel, carYear: carYear, partName: partName, inStock: false }
    }).done(function (response) {
        $(".parts-order-search-list").html(response);
    })
})


$(document).on("click", ".add-part", function () {
    var partId = $(this).attr("id");
    var inStock = $(this).data("in-stock");
    var partQuantity = $("#PartQuantity_" + partId).val();
    console.log(partId);
    $.ajax({
        url: "/Repair/AddPartToList",
        method: "POST",
        data: { partId: partId, partQuantity: partQuantity, inStock: inStock }
    }).done(function (response) {
        $(".parts-list-table").html(response);
    })
});

$(document).on("click", ".delete-part", function () {
    var partId = $(this).attr("id");
    var inStock = $(this).data("in-stock");
    console.log(partId);
    $.ajax({
        url: "/Repair/DeletePartFromList",
        method: "POST",
        data: { partId: partId, inStock: inStock }
    }).done(function (response) {
        $(".parts-list-table").html(response);
    })
});

$(document).on("click", "#finalize-repair-button", function () {
    window.location.href = '/Repair/FinalizeRepair';
})

$(document).on("click", "#make-order-button", function () {
    window.location.href = '/Repair/MakeOrder';
})

$(document).on("click", ".part-list-for-repair-arrow", function () {
    $(this).parent().parent().next().toggle();
})
