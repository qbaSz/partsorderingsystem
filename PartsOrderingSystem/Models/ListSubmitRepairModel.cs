﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class ListSubmitRepairModel
    {
        public List<SubmitRepairModel> RepairParts { get; set; }
    }
}