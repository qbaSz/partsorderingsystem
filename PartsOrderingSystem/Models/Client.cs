﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class Client
    {
        [Required(ErrorMessage = "Musisz wybrać klienta")]
        public int ClientId { get; set; }

        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Display(Name = "Adres e-mail")]
        public string EMail { get; set; }

        [Display(Name = "Telefon")]
        public string Phone { get; set; }
        public virtual ICollection<Repair> Repairs { get; set; }

    }
}