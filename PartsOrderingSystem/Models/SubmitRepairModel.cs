﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class SubmitRepairModel
    {
        public Repair Repair { get; set; }
        public List<RepairXPart> PartsList { get; set; }
    }
}