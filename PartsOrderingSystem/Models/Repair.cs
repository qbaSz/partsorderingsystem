﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class Repair
    {
        public int RepairId { get; set; }
        public int ClientId { get; set; }
        public string ApplicationUserId { get; set; }

        [Display(Name = "Data naprawy")]
        public DateTime RepairDate { get; set; }
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }
        public virtual Client Client { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<RepairXPart> RepairXParts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

    }
}