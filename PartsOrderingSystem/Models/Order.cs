﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int RepairId { get; set; }

        [Display(Name = "Data zamówienia")]
        public DateTime OrderDate { get; set; }
        public int StatusId { get; set; }

        [Display(Name = "Gotowość zamówienia")]
        public bool ClientAccepted { get; set; }
        public virtual Repair Repair { get; set; }
        public virtual ICollection<OrderXPart> OrderXParts { get; set; }

    }
}