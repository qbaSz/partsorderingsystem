﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class Status
    {
        public int StatusId { get; set; }

        [Display(Name = "Decyzja klienta")]
        public string Name { get; set; }

    }
}