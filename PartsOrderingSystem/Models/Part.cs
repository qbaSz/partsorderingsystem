﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class Part
    {
        public int PartId { get; set; }

        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Marka")]
        public string CarMake { get; set; }

        [Display(Name = "Model")]
        public string CarModel { get; set; }

        [Display(Name = "Rocznik")]
        public string CarYear { get; set; }

        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Display(Name = "W magazynie")]
        public bool InStock { get; set; }
        public virtual ICollection<OrderXPart> OrderXParts { get; set; }
        public virtual ICollection<RepairXPart> RepairXparts { get; set; }
    }
}