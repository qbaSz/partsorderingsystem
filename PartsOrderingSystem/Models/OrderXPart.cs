﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class OrderXPart
    {
        public int OrderXPartId { get; set; }
        public int PartId { get; set; }
        public int OrderId { get; set; }
        public decimal Total { get; set; }
        public int PartQuantity { get; set; }
        public virtual Part Part { get; set; }
        public virtual Order Order { get; set; }
    }
}