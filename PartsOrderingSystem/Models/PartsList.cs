﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class PartsList
    {
        public Part Part { get; set; }
        public int Quantity { get; set; }
    }
}