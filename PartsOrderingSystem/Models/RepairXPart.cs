﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartsOrderingSystem.Models
{
    public class RepairXPart
    {
        public int RepairXPartId { get; set; }
        public int PartId { get; set; }
        public int RepairId { get; set; }

        [Display(Name = "Ilość")]
        public int PartQuantity { get; set; }

        [Display(Name = "Wartość zakupu")]
        public decimal Total { get; set; }
        public virtual Part Part { get; set; }
        public virtual Repair Repair { get; set; }

    }
}