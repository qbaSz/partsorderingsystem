﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PartsOrderingSystem.Startup))]
namespace PartsOrderingSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
