﻿using Microsoft.AspNet.Identity;
using PartsOrderingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PartsOrderingSystem.Controllers
{
    
    [Authorize]
    public class RepairController : Controller
    {
        const string EMPTY_STRING = "";
        const string SESSION_PART_LIST = "PartList";
        const string SESSION_REPAIR = "Repair";
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Repair
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            return View();
        }

        public ActionResult RepairList()
        {
            string userName = User.Identity.Name;
            var repairs = db.Repairs.Where(r => r.ApplicationUser.UserName == userName).ToList();
            return View(repairs);
        }

        public ActionResult OrderList()
        {
            string userName = User.Identity.Name;
            var orders = db.Orders.Where(o => o.Repair.ApplicationUser.UserName == userName).ToList();
            return View(orders);
        }

        public ActionResult CreateRepair(Client client)
        {
            if (!ModelState.IsValid)
            {
                return View("New");
            }
            Repair repair = new Repair();
            repair.ClientId = client.ClientId;
            repair.Client = db.Clients.Where(c => c.ClientId == client.ClientId).First();
            repair.ApplicationUserId = User.Identity.GetUserId();
            repair.RepairDate = DateTime.Now;
            repair.RepairId = (db.Repairs.Count() == 0) ? 1 : db.Repairs.Max(r => r.RepairId) + 1;
            repair.StatusId = 2;
            ViewBag.RepairId = repair.RepairId;
            Session[SESSION_PART_LIST] = new List<RepairXPart>();
            Session[SESSION_REPAIR] = repair;
            return View();
        }

        [HttpPost]
        public ActionResult SubmitRepair()
        {
            var partList = (List<RepairXPart>)Session[SESSION_PART_LIST];
            return View(partList);
        }

        [HttpGet]
        public ActionResult MakeOrder()
        {
            return View();
        }

        public ActionResult FinalizeRepair()
        {
            var repair = (Repair)Session[SESSION_REPAIR];
            var partList = (List<RepairXPart>)Session[SESSION_PART_LIST];
            return View(new SubmitRepairModel() { Repair = repair, PartsList = partList });
        }

        public ActionResult EndRepairSequence()
        {
            var repair = (Repair)Session[SESSION_REPAIR];
            Order order = null;
            Repair newRepair = new Repair()
            {
                ApplicationUserId = repair.ApplicationUserId,
                ClientId = repair.ClientId,
                RepairId = repair.RepairId,
                StatusId = repair.StatusId,
                RepairDate = repair.RepairDate
            };
            var partList = (List<RepairXPart>)Session[SESSION_PART_LIST];
            var partListNotInStock = partList.Where(p => !p.Part.InStock).ToList();
            var partListInStock = partList.Where(p => p.Part.InStock).ToList();
            if(partListInStock.Count != 0)
            {
                foreach(RepairXPart rxp in partListInStock.ToList())
                {
                    RepairXPart rxpNew = new RepairXPart()
                    {
                        PartId = rxp.PartId,
                        RepairId = rxp.RepairId,
                        PartQuantity = rxp.PartQuantity,
                        Total = rxp.Total
                    };
                    db.RepairXParts.Add(rxpNew);
                }
            }
            if(partListNotInStock.Count != 0)
            {
                order = new Order()
                {
                    RepairId = repair.RepairId,
                    OrderDate = repair.RepairDate,
                    ClientAccepted = false
                };
                order = db.Orders.Add(order);
                foreach(RepairXPart rxp in partListNotInStock.ToList())
                {
                    db.OrderXParts.Add(new OrderXPart()
                    {
                        PartId = rxp.PartId,
                        OrderId = order.OrderId,
                        PartQuantity = rxp.PartQuantity,
                        Total = rxp.Total
                    });
                }
            }
            db.Repairs.Add(newRepair);
            db.SaveChanges();
            if (order != null) ActivitiController.RESTRunConfirmOrderProcess(order.OrderId);
            ActivitiController.RESTRunConfirmRepairProcess(newRepair.RepairId);
            return RedirectToAction("RepairList");
        }

        public ActionResult FindClient(string surname, string email)
        {
            var clients = db.Clients.AsQueryable();
            Console.WriteLine(email);
            if (email != EMPTY_STRING)
                clients = clients.Where(c => c.EMail == email);
            if (surname != EMPTY_STRING)
                clients = clients.Where(c => c.Surname.Equals(surname));
            return View("ClientsList", clients.ToList());
        }

        public ActionResult FindPart(string carMake, string carModel, string carYear, string partName, bool inStock)
        {
            var parts = db.Parts.Where(p => p.InStock == inStock).AsQueryable();
            if(carMake != EMPTY_STRING)
            {
                parts = parts.Where(p => p.CarMake.ToUpper().Contains(carMake.ToUpper()));
            }
            if(carModel != EMPTY_STRING)
            {
                parts = parts.Where(p => p.CarModel.ToUpper().Contains(carModel.ToUpper()));
            }
            if(carYear != EMPTY_STRING)
            {
                parts = parts.Where(p => p.CarYear.Contains(carYear));
            }
            if(partName != EMPTY_STRING)
            {
                parts = parts.Where(p => p.Name.ToUpper().Contains(partName.ToUpper()));
            }

            return View("PartsList", parts.ToList());
        }

        public ActionResult AddPartToList(int partId, int partQuantity, bool inStock)
        {
            var partList = (List<RepairXPart>)Session[SESSION_PART_LIST];
            var rxp = partList.Where(p => p.PartId == partId).FirstOrDefault();
            if(rxp == null)
            {
                var part = db.Parts.Where(p => p.PartId == partId).First();
                var repair = (Repair)Session[SESSION_REPAIR];
                rxp = new RepairXPart() { Part = part, PartId = part.PartId, RepairId = repair.RepairId, PartQuantity = partQuantity, Total = partQuantity * part.Price };
                partList.Add(rxp);
            }
            else
            {
                rxp.PartQuantity += partQuantity;
                rxp.Total = rxp.PartQuantity * rxp.Part.Price;
            }
            Session[SESSION_PART_LIST] = partList;
            return PartialView(partList.Where(p => p.Part.InStock == inStock).ToList());
        }

        public ActionResult DeletePartFromList(int partId, bool inStock)
        {
            var partList = (List<RepairXPart>)Session[SESSION_PART_LIST];
            var part = partList.Where(p => p.PartId == partId).First();
            partList.Remove(part);
            Session[SESSION_PART_LIST] = partList;
            return PartialView("AddPartToList", partList.Where(p => p.Part.InStock == inStock).ToList());
        }

        [HttpGet]
        public ActionResult RegisterClient()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegisterClient(Client client)
        {
            if (!ModelState.IsValid) {
                return View();
            }

            db.Clients.Add(client);
            db.SaveChanges();
            return View("New");
        }
    }
}