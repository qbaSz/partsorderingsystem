﻿using PartsOrderingSystem.Models;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PartsOrderingSystem.Controllers
{
    public class ActivitiController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Activiti
        public ActionResult Index()
        {
            return View();
        }

        public string ConfirmRepair(int repairId)
        {
            var repair = db.Repairs.Where(r => r.RepairId == repairId).First();
            repair.StatusId = 1;
            db.SaveChanges();
            return "repair confirmed";
        }

        public string RejectRepair(int repairId)
        {
            var repair = db.Repairs.Where(r => r.RepairId == repairId).First();
            repair.StatusId = 3;
            db.SaveChanges();
            return "repair rejected";
        }

        public string ConfirmOrder(int orderId)
        {
            var order = db.Orders.Where(o => o.OrderId == orderId).First();
            order.ClientAccepted = true;
            var orderXParts = db.OrderXParts.Where(oxp => oxp.OrderId == orderId);
            foreach(OrderXPart oxp in orderXParts)
            {
                var part = db.Parts.Where(p => p.PartId == oxp.PartId).First();
                part.InStock = true;
            }
            db.SaveChanges();
            return "order confirmed";
        }

        public static void RESTRunConfirmOrderProcess(int orderId)
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/runtime/process-instances", Method.POST);
            request.AddJsonBody(new
            {
                processDefinitionId = "confirmOrder:1:43",
                businessKey = "Order confirmation",
                variables = new[]
                {
                    new { name = "orderId", value = orderId.ToString() }
                }
            });
            IRestResponse response = client.Execute(request);
        }

        public static void RESTRunConfirmRepairProcess(int repairId)
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/runtime/process-instances", Method.POST);
            request.AddJsonBody(new
            {
                processDefinitionId = "confirmRepair:1:47",
                businessKey = "Repair confirmation",
                variables = new[]
                {
                    new { name = "repairId", value = repairId.ToString() }
                }
            });
            IRestResponse response = client.Execute(request);
        }

        public ActionResult REST()
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");

            var request = new RestRequest("service/repository/process-definitions", Method.GET);
            IRestResponse response = client.Execute(request);

            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        public ActionResult RESTUploadDeploymentOrder()
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/repository/deployments", Method.POST);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddFile("content", "C:/Users/Jakub Szubert/Documents/Visual Studio 2017/Projects/PartsOrderingSystem/PartsOrderingSystem/Content/Digrams/confirmOrder.bpmn20.xml.zip");
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        public ActionResult RESTUploadDeploymentRepair()
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/repository/deployments", Method.POST);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddFile("content", "C:/Users/Jakub Szubert/Documents/Visual Studio 2017/Projects/PartsOrderingSystem/PartsOrderingSystem/Content/Digrams/confirmRepair.bpmn20.xml.zip");
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        [HttpGet]
        public ActionResult RESTShowDeployments(string id)
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/repository/deployments/" + id, Method.GET);
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        public ActionResult RESTShowProcessInstances()
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/runtime/process-instances", Method.GET);
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        public ActionResult RESTRunProcess(string id)
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/runtime/process-instances", Method.POST);
            request.AddJsonBody(new
            {
                processDefinitionId = "confirmRepair:1:47",
                businessKey = "Order confirmation",
                variables = new[]
                {
                    new { name = "repairId", value = id }
                }
            });
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        public ActionResult RESTListVariablesForProcess(string id)
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/runtime/process-instances/" + id + "/variables", Method.POST);
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        public ActionResult RESTActivateProcessDefinition(string id)
        {
            var client = new RestClient("http://localhost:8080/activiti-rest/");
            client.Authenticator = new HttpBasicAuthenticator("kermit", "kermit");
            var request = new RestRequest("service/repository/process-definitions/" + id, Method.PUT);
            request.AddJsonBody(new
            {
                action = "activate",
                includeProcessInstances = true,
                date = DateTime.Now
            });
            IRestResponse response = client.Execute(request);
            var result = response.Content;

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("REST", JValue.Parse(result).ToString(Newtonsoft.Json.Formatting.Indented));
        }

        [HttpGet]
        public ActionResult RESTActivitiCheck(int id)
        {
            ActivitiController.RESTRunConfirmOrderProcess(id);
            return RedirectToAction("RESTRedirect", new { id = id });
        }

        public ActionResult RESTRedirect(int id)
        {
            return View("REST", id);
        }
    }
}