SET IDENTITY_INSERT [dbo].[Clients] ON
INSERT INTO [dbo].[Clients] ([ClientId], [Name], [Surname], [EMail], [Phone]) VALUES (1, N'Beata', N'Szyd�o', N'beata@broszka.com', N'123456789')
INSERT INTO [dbo].[Clients] ([ClientId], [Name], [Surname], [EMail], [Phone]) VALUES (2, N'Jan', N'Janowski', N'jan@jan.com', N'987999777')
INSERT INTO [dbo].[Clients] ([ClientId], [Name], [Surname], [EMail], [Phone]) VALUES (3, N'Maciej', N'Nowak', N'nowak.maciej@mail.com', N'782554789')
SET IDENTITY_INSERT [dbo].[Clients] OFF

SET IDENTITY_INSERT [dbo].[Parts] ON
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (1, N'Lusterko', N'Opel', N'Astra', N'1997', CAST(60.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (2, N'Alternator', N'Volvo', N'S60', N'2004', CAST(400.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (3, N'Antena', N'Volvo', N'S60', N'2004', CAST(100.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (4, N'Antena', N'Opel', N'Astra', N'1997', CAST(40.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (5, N'Reflektor przedni prawy', N'Opel', N'Astra', N'1997', CAST(70.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (6, N'Reflektor przedni lewy', N'Opel', N'Astra', N'1997', CAST(70.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (7, N'Kierunkowskaz prawy', N'Volvo', N'S60', N'2004', CAST(120.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (8, N'Lusterko', N'Skoda', N'Fabia', N'2004', CAST(300.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (9, N'Pompka spryskiwacza', N'Skoda', N'Octavia', N'2012', CAST(50.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (10, N'Manetka kierunkowskaz�w', N'Honda', N'Civic', N'2001', CAST(20.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (11, N'Termostat', N'Honda', N'Civic', N'2001', CAST(60.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (12, N'Termostat', N'Opel', N'Astra', N'1997', CAST(30.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (13, N'Antena', N'Skoda', N'Fabia', N'2004', CAST(80.00 AS Decimal(18, 2)), 1)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (14, N'Zbiornik spryskiwacza', N'Opel', N'Astra', N'1997', CAST(35.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (15, N'Filtr paliwa', N'Opel', N'Astra', N'2011', CAST(70.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (16, N'Filtr kabinowy', N'Opel', N'Astra', N'2011', CAST(35.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (17, N'Filtr kabinowy', N'Opel', N'Vectra', N'2002', CAST(38.00 AS Decimal(18, 2)), 0)
INSERT INTO [dbo].[Parts] ([PartId], [Name], [CarMake], [CarModel], [CarYear], [Price], [InStock]) VALUES (18, N'Filtr kabinowy', N'Skoda', N'Fabia', N'2011', CAST(40.00 AS Decimal(18, 2)), 0)
SET IDENTITY_INSERT [dbo].[Parts] OFF

SET IDENTITY_INSERT [dbo].[Status] ON
INSERT INTO [dbo].[Status] ([StatusId], [Name]) VALUES (1, N'Potwierdzono')
INSERT INTO [dbo].[Status] ([StatusId], [Name]) VALUES (2, N'Niepotwierdzono')
INSERT INTO [dbo].[Status] ([StatusId], [Name]) VALUES (3, N'Odrzucono')
SET IDENTITY_INSERT [dbo].[Status] OFF